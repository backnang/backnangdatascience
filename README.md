Dieses Repository enthält Daten zu einem regelmäßigen Treffen von Data Scientists in
Backnang, Deutschland: https://www.meetup.com/backnang-data-science/

Das Meiste inhalt sollte unter [events](events) sein.

Unsere Meeting-Notizen: von aktuell bis damals

# 2024

## Dezember

- Link: https://www.meetup.com/backnang-data-science/events/304538214/


## November

- Link: https://www.meetup.com/backnang-data-science/events/304484555/

- Teilnehmerzahl: 3 (Nicht bei Meeup angemelded)

Diskussionspunkte in der Vorstellungsrunde

- Arbeiten mit Daten: Quantitative vs. qualitative Datenanalyse

- Benutzerbewertung für eine Carsharing-App: Nutzung von Telemetrie und Bildbewertung.

- Qualitätssicherung in der Produktion: Identifizierung fehlender Teile, auffälliger
  Komponenten oder Prozesse. Automatische Bildauswertung zur Ermittlung von Herstellern.

- Einführung neuer Tools: Schwierigkeiten bei der Nutzung durch Kollegen.

Paul Rougieux stellte zwei Softwarepakete auf PyPI vor:

- eu-cbm-hat: https://pypi.org/project/eu-cbm-hat/ Dieses Paket dient zur Prognose von
  Kohlenstoffemissionen und -entnahmen in Wäldern.

- biotrade: https://pypi.org/project/biotrade/ Dieses Paket wird im Rahmen des Forest
  Observatory der Europäischen Kommission
  (https://forest-observatory.ec.europa.eu/commodities) eingesetzt.


## März

Teilnehmerzahl: 0

Es war geplant, zu erklären, wie man gleitende Mittelwerte (rolling means) und
Interpolation mit pandas transform berechnet. Diese Techniken werden in
Beispiel-Python-Skripten in diesem Verzeichnis [events/202403](events/202403)
veranschaulicht.


