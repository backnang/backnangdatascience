
This document is here to draft the meetup site content

# Social Media Links

- Linkedin Group https://www.linkedin.com/groups/13118782/


# Meetup page content

## Über Uns

Lass uns gemeinsam mit Daten und Code die Welt ein bisschen besser verstehen. Wir
schlagen vor, erste Prinzipien, datenwissenschaftliche Methoden und Programmierwerkzeuge
zu kombinieren, um eine umfassende Analyse durchzuführen. Wir begrüßen den freundlichen
Austausch von Wissen, insbesondere von introvertierten und neugierigen Personen. Seien
Sie nicht schüchtern. Reden Sie auch Unsinn. Fehler sind eine gute Möglichkeit,
voneinander zu lernen. Experten streben danach, ihr Wissen besser erklären zu können.
Lernende streben danach, bessere Fragen zu stellen. Die richtige Frage zu stellen, ist
eine Kunst.



