---
title: "Data Science Connect: Offener Treffpunkt und Wissensaustausch"
date: 2024-12-16
date-format: "[Montag] D [Dezember] YYYY [18h45-20h15]"
format: pdf
geometry: top=0cm, left=1.5cm, right=1.5cm, bottom=0cm
fontsize: 12pt
---

<!-- Render with

penv
cd ~/rp/backnangdatascience/events/202412 && quarto render bds_flyer_202412.qmd

Alternative separated images and centered

Layout two columns
::: {layout-ncol=2}

![](../pics/data_science_languages.png){width=300}

![](../pics/bds_qr.png){width=200}

:::

-->

![](../pics/data_science_languages.png){width=300 fig-align="centre"}

Tauchen Sie ein in die Welt der Datenwissenschaft bei unserem Networking-Event 
Data Science Connect. Dieses Treffen bietet eine entspannte Atmosphäre, um sich 
mit Gleichgesinnten auszutauschen und spontane Interaktionen zu fördern. Egal, 
ob Sie ein erfahrener Data Scientist oder ein neugieriger Anfänger sind, hier 
finden Sie wertvolle Einblicke in die verschiedenen Programmiersprachen und 
Tools der Datenwissenschaft, wie Python, R, Matlab, Julia, Java, Fortran, SAS, 
SQL und mehr. Erleben Sie inspirierende Gespräche, teilen Sie Ihre Erfahrungen 
und erweitern Sie Ihr Netzwerk in einer informellen Umgebung. Bringen Sie Ihre 
Fragen mit, diskutieren Sie aktuelle Trends und lassen Sie sich von der 
Vielfalt der Ansätze in der Datenanalyse inspirieren.

Tagesordnung

1. **Vorstellungsrunde** - Jeder Teilnehmer stellt kurz vor, wie er Daten und Analysen in seiner Arbeit nutzt.
2. Technische Diskussion über die Implementierung eines .**Data-Science-Softwarepakets**
3. **Diskussion sozialer Aspekte** wie kontinuierliche Weiterbildung, Vertrauensaufbau in Data-Science-Tools und Change Management.
4. Austausch über **Lernziele** für zukünftige Meetings

Seien Sie dabei und gestalten Sie die Zukunft der Datenwissenschaft mit!

- Quelle des Bildes 
  https://www.birchwoodu.org/top-programming-languages-for-data-scientists/

- Meetup link https://www.meetup.com/de-DE/backnang-data-science/

![](../pics/bds_qr.png){width=100 fig-align="centre"}
