"""The purpose of this script is to demonstrate the use of pandas transform

Documentation:

- https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.transform.html

"""

import pandas
import numpy as np

# Sum by groups
df = pandas.DataFrame({"i": ["a", "a", "b", "b", "b"], "x": range(1, 6)})
df["y"] = df.groupby("i")["x"].transform("sum")


# Interpolate
df = pandas.DataFrame(
    {
        "i": ["a", "a", "a", "a", "b", "b", "b"],
        "year": [2020, 2021, 2022, 2023, 2020, 2021, 2022],
        "x": [1, np.nan, np.nan, 4, 2, 4, np.nan],
    }
)
df["y"] = df.groupby("i")["x"].transform(pandas.Series.interpolate)

# Compute a temporal difference
df["y_diff"] = df.groupby("i")["y"].transform(lambda x: x.diff())
