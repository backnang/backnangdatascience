"""Compute a yearly rolling mean"""
import pandas
from matplotlib import pyplot as plt

# Yearly rolling mean of a monthly time series
li = list(range(15))
df = pandas.DataFrame(
    {
        "x": li + list(reversed(li)) + li,
        "month": 3 * list(range(1, 13)) + list(range(1, 10)),
        "year": 12 * [2020] + 12 * [2021] + 12 * [2022] + 9 * [2023],
    }
)
df["date"] = pandas.to_datetime(df["month"]+df["year"]*100, format='%Y%m')
df["y"] = df["x"].rolling(13).mean()
df[["date", "x", "y"]].plot(x="date")
plt.show()

# Start from the beginning
df["y"] = df["x"].rolling(13, min_periods=1).mean()
df[["date", "x", "y"]].plot(x="date")
plt.show()


